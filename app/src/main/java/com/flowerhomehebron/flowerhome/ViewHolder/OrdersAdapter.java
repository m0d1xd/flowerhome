package com.flowerhomehebron.flowerhome.ViewHolder;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.flowerhomehebron.flowerhome.Interface.ItemClickListener;
import com.flowerhomehebron.flowerhome.R;
import com.flowerhomehebron.flowerhome.model.Order;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView tv_itemName, tv_itemPrice;
    public ImageView img_item;
    private ItemClickListener itemClickListener;

    public void setTv_itemName(TextView tv_itemName) {
        this.tv_itemName = tv_itemName;

    }

    public OrderViewHolder(View itemView) {
        super(itemView);
        tv_itemName = (TextView) itemView.findViewById(R.id.order_itemName);
        tv_itemPrice = (TextView) itemView.findViewById(R.id.order_itemPrice);
        img_item = (ImageView) itemView.findViewById(R.id.img_item_order);
    }

    @Override
    public void onClick(View v) {


    }
}

public class OrdersAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    private List<Order> listData = new ArrayList<>();
    Context context;

    public OrdersAdapter(List<Order> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.orders_layout, parent, false);
        return new OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        TextDrawable drawable = TextDrawable.builder().buildRound("" + listData.get(position).getQuantity(), Color.RED);
        holder.img_item.setImageDrawable(drawable);
        Locale local = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(local);
        int totalp = (Integer.parseInt(listData.get(position).getPrice())) * Integer.parseInt(listData.get(position).getQuantity());
        int total = Integer.parseInt(listData.get(position).getPrice());

        holder.tv_itemPrice.setText(fmt.format(total) + "\t* " + listData.get(position).getQuantity() + "\t = " + fmt.format(totalp));
        holder.tv_itemName.setText(listData.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
