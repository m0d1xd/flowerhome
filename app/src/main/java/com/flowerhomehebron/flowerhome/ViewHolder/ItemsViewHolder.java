package com.flowerhomehebron.flowerhome.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flowerhomehebron.flowerhome.Interface.ItemClickListener;
import com.flowerhomehebron.flowerhome.R;


public class ItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView tv_itemname;
    public ImageView img_item;

    private ItemClickListener itemClickListener;


    public ItemsViewHolder(View itemView) {
        super(itemView);


        tv_itemname = (TextView) itemView.findViewById(R.id.tv_itemNmae);
        img_item = (ImageView) itemView.findViewById(R.id.img_item);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

}
