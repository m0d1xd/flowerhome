package com.flowerhomehebron.flowerhome.ViewHolder;

import android.support.v7.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flowerhomehebron.flowerhome.Interface.ItemClickListener;
import com.flowerhomehebron.flowerhome.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textGategoryName;
    public ImageView imageGategory;
    private ItemClickListener itemClickListener;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        textGategoryName = itemView.findViewById(R.id.tv_categoryNmae);
        imageGategory = itemView.findViewById(R.id.img_gategory);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
