package com.flowerhomehebron.flowerhome.Activity.Orders;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;

import com.flowerhomehebron.flowerhome.Database.Database;
import com.flowerhomehebron.flowerhome.R;
import com.flowerhomehebron.flowerhome.ViewHolder.OrdersAdapter;
import com.flowerhomehebron.flowerhome.model.Order;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference request;

    TextView tv_totalPrice;
    Button btn_placeOrder;

    List<Order> cart = new ArrayList<>();
    OrdersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        //Init Firebases
        database = FirebaseDatabase.getInstance();
        request = database.getReference("Requests");
        recyclerView = (RecyclerView) findViewById(R.id.List_cart);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        tv_totalPrice = (TextView) findViewById(R.id.tv_total);
        btn_placeOrder = (Button) findViewById(R.id.btn_PlaceOrder);


        LoadListItems();

    }

    private void LoadListItems() {
        cart = new Database(this).getOrders();
        adapter = new OrdersAdapter(cart, this);
        recyclerView.setAdapter(adapter);

        int total = 0;
        for (Order order : cart) {
            total += (Integer.parseInt(order.getQuantity()) * Integer.parseInt(order.getPrice()));
            Locale local = new Locale("he", "he_IL");
            NumberFormat fmt = NumberFormat.getCurrencyInstance(local);

        }

        tv_totalPrice.setText(String.valueOf(total));
    }
}
