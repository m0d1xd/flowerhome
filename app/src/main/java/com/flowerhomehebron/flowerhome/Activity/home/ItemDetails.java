package com.flowerhomehebron.flowerhome.Activity.home;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.flowerhomehebron.flowerhome.Database.Database;
import com.flowerhomehebron.flowerhome.R;
import com.flowerhomehebron.flowerhome.model.Items;
import com.flowerhomehebron.flowerhome.model.Order;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ItemDetails extends AppCompatActivity {
    TextView tv_itemName, tv_itemPrice, tv_itemDescription;
    ImageView img_item;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btn_toCart;
    ElegantNumberButton btn_quantity;
    String ItemID = "";
    FirebaseDatabase database;
    DatabaseReference items;
    Items item;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        items = database.getReference("items");

        //Init Views

        btn_quantity = (ElegantNumberButton) findViewById(R.id.btn_quantity);

        btn_toCart = (FloatingActionButton) findViewById(R.id.btn_toCart);

        btn_toCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToCart(new Order(ItemID,
                        item.getName(),
                        btn_quantity.getNumber(),
                        item.getPrice(),
                        item.getDiscount()));
                //Toast.makeText(getApplicationContext(), "Added To Cart", Toast.LENGTH_SHORT).show();
            }
        });


        tv_itemName = (TextView) findViewById(R.id.tv_itemname);
        tv_itemPrice = (TextView) findViewById(R.id.tv_itemPrice);
        tv_itemDescription = (TextView) findViewById(R.id.tv_item_Discription);
        img_item = (ImageView) findViewById(R.id.img_item);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbar);


        //Getting Food id from previous intent
        if (getIntent() != null) {
            ItemID = getIntent().getStringExtra("ItemID");
        }
        if (!ItemID.isEmpty() && ItemID != null) {

            getItemDetails(ItemID);
        }

    }

    private void getItemDetails(String itemID) {
        items.child(itemID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                item = dataSnapshot.getValue(Items.class);
                Picasso.with(getBaseContext()).load(item.getImageUrl()).into(img_item);
                collapsingToolbarLayout.setTitle(item.getName());
                tv_itemPrice.setText(item.getPrice());
                tv_itemName.setText(item.getName());
                tv_itemDescription.setText(item.getDescription());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
