package com.flowerhomehebron.flowerhome.Activity.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.flowerhomehebron.flowerhome.Activity.Orders.CartActivity;
import com.flowerhomehebron.flowerhome.Common.Common;
import com.flowerhomehebron.flowerhome.Interface.ItemClickListener;
import com.flowerhomehebron.flowerhome.MainActivity;
import com.flowerhomehebron.flowerhome.R;
import com.flowerhomehebron.flowerhome.ViewHolder.CategoryViewHolder;
import com.flowerhomehebron.flowerhome.model.Category;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView.LayoutManager layoutManager;
    private TextView name, email;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference mDatabaseRefrence;
    private RecyclerView recyclerView;
    private FirebaseUser firebaseUser;
    FirebaseRecyclerAdapter<Category, CategoryViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseRefrence = firebaseDatabase.getReference("Category");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent cart = new Intent(HomeActivity.this, CartActivity.class);
                startActivity(cart);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        name = headerView.findViewById(R.id.tv_UserName);
        email = headerView.findViewById(R.id.tv_email);

        name.setText(Common.CurrentUser.getName());
        email.setText(Common.CurrentUser.getEmail());
//

        //Loading Category from Firebase

        recyclerView = findViewById(R.id.recycle_menu);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        LoadCategories();

    }

    private void LoadCategories() {
        adapter = new FirebaseRecyclerAdapter<Category, CategoryViewHolder>
                (Category.class, R.layout.category_item, CategoryViewHolder.class, mDatabaseRefrence) {

            @Override
            protected void populateViewHolder(CategoryViewHolder viewHolder, Category model, int position) {
                Picasso.with(getBaseContext()).load(model.getImageUrl()).into(viewHolder.imageGategory);
                viewHolder.textGategoryName.setText(model.getName());
                final Category clickitem = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Toast.makeText(HomeActivity.this, "" + clickitem.getName() + " is Clicked", Toast.LENGTH_SHORT).show();
                        //Get Catagory ID and pass it to new Activity;
                        Intent ItemsList = new Intent(HomeActivity.this, ItemList.class);
                        ItemsList.putExtra("CatagoryID", adapter.getRef(position).getKey());
                        startActivity(ItemsList);

                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_LogOut) {
            // Handle the camera action
            SignOut();

        } else if (id == R.id.nav_userpanel) {
            //   UserPanel();
        }
//        } else if (id == R.id.nav_slideshow) {
//
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }


    public void SignOut() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(HomeActivity.this, MainActivity.class));
        Toast.makeText(getApplicationContext(), "Good Bye", Toast.LENGTH_SHORT).show();
        finish();
    }
}
