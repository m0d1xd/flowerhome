package com.flowerhomehebron.flowerhome.Activity.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.flowerhomehebron.flowerhome.Interface.ItemClickListener;
import com.flowerhomehebron.flowerhome.R;
import com.flowerhomehebron.flowerhome.ViewHolder.ItemsViewHolder;
import com.flowerhomehebron.flowerhome.model.Items;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class ItemList extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseDatabase database;
    DatabaseReference itemslist;
    String CatID = "";
    FirebaseRecyclerAdapter<Items, ItemsViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        //Init FirebaseDB
        database = FirebaseDatabase.getInstance();
        itemslist = database.getReference("items");
        recyclerView = (RecyclerView) findViewById(R.id.recycle_itemlist);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Getting User last sellection For Catagory
        if (getIntent() != null) {
            CatID = getIntent().getStringExtra("CatagoryID");
        }
        if (!CatID.isEmpty() && CatID != null) {
            LoadItems(CatID);
        }

    }

    private void LoadItems(String catID) {
        adapter = new FirebaseRecyclerAdapter<Items, ItemsViewHolder>(Items.class,
                R.layout.list_items,
                ItemsViewHolder.class,
                itemslist.orderByChild("CatagoryID").equalTo(catID)) {


            @Override
            protected void populateViewHolder(ItemsViewHolder viewHolder, Items model, int position) {
                viewHolder.tv_itemname.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImageUrl()).into(viewHolder.img_item);


                final Items clickitem = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        // Toast.makeText(ItemList.this, "" + clickitem.getName() + " is Clicked", Toast.LENGTH_SHORT).show();
                        //Get Catagory ID and pass it to new Activity;
                        //Start new Activity for selected item
                        Intent itemDetails = new Intent(ItemList.this, ItemDetails.class);
                        itemDetails.putExtra("ItemID", adapter.getRef(position).getKey());
                        startActivity(itemDetails);

                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);

    }
}
