package com.flowerhomehebron.flowerhome.model;

public class Items {
    private String Name, ImageUrl, Description, Price, Discount, CatagoryID;

    public Items() {
    }

    public Items(String Name, String ImageUrl, String Description, String Price, String Discount, String CatagoryID) {
        this.Name = Name;
        this.ImageUrl = ImageUrl;
        this.Description = Description;
        this.Price = Price;
        this.Discount = Discount;
        this.CatagoryID = CatagoryID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String ImageUrl) {
        this.ImageUrl = ImageUrl;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String Discount) {
        this.Discount = Discount;
    }

    public String getCatagoryID() {
        return CatagoryID;
    }

    public void setCatagoryID(String CatagoryID) {
        this.CatagoryID = CatagoryID;
    }

}
