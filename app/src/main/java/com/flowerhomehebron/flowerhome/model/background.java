package com.flowerhomehebron.flowerhome.model;

public class background {

    private String bg_id, ImageUrl;

    public background(String bg_id, String imageUrl) {
        this.bg_id = bg_id;
        this.ImageUrl = imageUrl;
    }

    public background() {
    }

    public String getBg_id() {
        return bg_id;
    }

    public void setBg_id(String bg_id) {
        this.bg_id = bg_id;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "background{" +
                "bg_id='" + bg_id + '\'' +
                ", ImageUrl='" + ImageUrl + '\'' +
                '}';
    }
}
