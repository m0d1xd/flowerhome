package com.flowerhomehebron.flowerhome.model;

public class User {
    private String Name, Address, Email, Uid, Phone, Password;

    public User() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String Uid) {
        this.Uid = Uid;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }


    public void applyChanges(User data) {
        this.Name = (data.getName());
        this.Uid = (data.getUid());
        this.Password = (data.getPassword());
        this.Address = (data.getAddress());
        this.Phone = (data.getPhone());
        this.Email = data.getEmail();
    }

    @Override
    public String toString() {
        return "User{" +
                "Name='" + Name + '\'' +
                ", Address='" + Address + '\'' +
                ", Email='" + Email + '\'' +
                ", Uid='" + Uid + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Password='" + Password + '\'' +
                '}';
    }
}
