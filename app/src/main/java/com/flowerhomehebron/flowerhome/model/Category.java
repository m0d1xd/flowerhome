package com.flowerhomehebron.flowerhome.model;

public class Category {
    private String Name, ImageUrl;

    public Category() {
    }

    public Category(String Name, String ImageUrl) {
        this.Name = Name;
        this.ImageUrl = ImageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String ImageUrl) {
        this.ImageUrl = ImageUrl;
    }
}
