package com.flowerhomehebron.flowerhome;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.flowerhomehebron.flowerhome.model.background;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class WelcomeScreen extends AppCompatActivity {

    final String TAG = "Welcome";
    ImageView img;

    //TODO This i will make for announcements Later
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        img = findViewById(R.id.img_welcome);

        FirebaseDatabase mFirebaseDataBase = FirebaseDatabase.getInstance();
        final DatabaseReference mDatabaseReference = mFirebaseDataBase.getReference("backgrounds");


        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                background bg = dataSnapshot.child(getRandomNumber()).getValue(background.class);
                if (bg != null) {
                    Log.d(TAG, "onDataChange: " + bg.toString());

                    Glide.with(getApplicationContext()).load(bg.getImageUrl()).into(img);

                } else {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        StartApp();
    }

    private String getRandomNumber() {
        //Note that general syntax is Random().nextInt(n)
        //It results in range 0-4
        //So it should be equal to number of images in images[] array
        return new Random().nextInt(3) + "";
    }

    public void StartApp() {
        Thread welcomeThread = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(3000);  //Delay of 10 seconds
                } catch (Exception e) {

                } finally {

                    Intent i = new Intent(WelcomeScreen.this,
                            MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        welcomeThread.start();


    }
}
