package com.flowerhomehebron.flowerhome.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.flowerhomehebron.flowerhome.model.Order;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteAssetHelper {

    static private final String DB_Name = "FlowerHome.db";
    static private final int DB_VER = 1;

    //
    private static final String TABLE_ORDER_DETAILS = "OrderDetails";
    private static final String TAG = "";
    // OrderDetails Table Columns
    private static final String KEY_ORDER_ProductID = "ProductID";
    private static final String KEY_ORDER_PRODUCTNAME = "ProductName";
    private static final String KEY_ORDER_QUANTITY = "Quantity";
    private static final String KEY_ORDER_PRICE = "Price";
    private static final String KEY_ORDER_DISCOUNT = "Discount";


    public Database(Context context) {
        super(context, DB_Name, null, DB_VER);
    }

    public void addToCart(Order order) {

        //Create and/or open the database for writing
        SQLiteDatabase db = getReadableDatabase();
        db.beginTransaction();


        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_ORDER_ProductID, order.getProductID());
            values.put(KEY_ORDER_PRODUCTNAME, order.getProductName());
            values.put(KEY_ORDER_QUANTITY, order.getQuantity());
            values.put(KEY_ORDER_PRICE, order.getPrice());
            values.put(KEY_ORDER_DISCOUNT, order.getDiscount());

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_ORDER_DETAILS, null, values);

            db.setTransactionSuccessful();
            //TRANSACTION COMPLETED
            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add Order to database");
        } finally {
            db.endTransaction();
            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
        }

//        String query = String.format("INSERT INTO OrderDetails(ProductID,ProductName,Quantity,Price,Discount) VALUES('%s','%s','%s','%s','%s');",
//                order.getProductID(),
//                order.getProductName(),
//                order.getQuantity(),
//                order.getPrice(),
//                order.getDiscount()
//        );
//
//        db.execSQL(query);


        // Create and/or open the database for writing
//        SQLiteDatabase db = getWritableDatabase();
//
        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
//        db.beginTransaction();
//        try {
//            // The user might already exist in the database (i.e. the same user created multiple posts).
//            long userId = addOrUpdateUser(post.user);
//
//            ContentValues values = new ContentValues();
//            values.put(KEY_POST_USER_ID_FK, userId);
//            values.put(KEY_POST_TEXT, post.text);
//
//            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
//            db.insertOrThrow(TABLE_POSTS, null, values);
//            db.setTransactionSuccessful();
//        } catch (Exception e) {
//            Log.d(TAG, "Error while trying to add post to database");
//        } finally {
//            db.endTransaction();
//        }


    }

    public void clearCart() {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetails");
        db.execSQL(query);
    }

    public List<Order> getOrders() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"ProductID", "ProductName", "ProductName", "Quantity", "Price", "Discount"};
        String sqlTable = "OrderDetails";
        qb.setTables(sqlTable);
        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
        final List<Order> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                result.add(new Order(c.getString(c.getColumnIndex("ProductID")),
                                c.getString(c.getColumnIndex("ProductName")),
                                c.getString(c.getColumnIndex("Quantity")),
                                c.getString(c.getColumnIndex("Price")),
                                c.getString(c.getColumnIndex("Discount"))
                        )
                );
            } while (c.moveToNext());
        }
        return result;
    }

}
